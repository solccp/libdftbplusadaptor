#include "DFTBPlusInputReader.h"

#include <QFile>
#include <QTextStream>

#include <QDebug>

#include "hsd-cpp/hsd.h"
#include "DFTBPlusInput.h"

namespace DFTBPlus
{

DFTBPlusInputReader::DFTBPlusInputReader(QObject *parent) :
    QObject(parent)
{
}

QString DFTBPlusInputReader::fileName() const
{
    QFile *file = qobject_cast<QFile *>(m_device);
    return file ? file->fileName() : QString();
}

bool DFTBPlusInputReader::read(DFTBPlusInput *input)
{
    if (!m_device->isOpen())
    {
        if (!m_device->open(QIODevice::ReadOnly))
        {
            return false;
        }
    }

    QTextStream stream(m_device);

    QString str = stream.readAll();

    std::string stdstr = str.toStdString();

    HSD::Node node = HSD::parseString(stdstr);

    if (!node.IsDefined() || node.IsNull())
    {
        return false;
    }

    if (node["Geometry"].IsDefined())
    {
        if (node["Geometry"].MethodName() == "GenFormat")
        {
            GenFormatGeometryNode gen;
            QStringList str;
//            qDebug() << node["Geometry"].IsScalarList();
//            for(int i=0; i<node["Geometry"].size();++i)
//            {
//                qDebug() << QString::fromStdString( node["Geometry"][i].as<std::string>());
//            }
            std::size_t index = 0;
            str.append(QString("%1 %2").arg(QString::fromStdString(node["Geometry"][index].as<std::string>()))
                    .arg(QString::fromStdString(node["Geometry"][index+1].as<std::string>())));
            index += 2;
            QStringList elements;
            for(std::size_t i=index; i<node["Geometry"].size();++i)
            {
                QString item = QString::fromStdString(node["Geometry"][i].as<std::string>());
                if (item[0].isLetter())
                {
                    elements.append(item);
                }
                else
                {
                    break;
                }
            }
            index += elements.size();
            str.append(elements.join(" "));

            QStringList line;
            while(index < node["Geometry"].size())
            {
                QString item = QString::fromStdString(node["Geometry"][index].as<std::string>());
                index++;
                line.append(item);
                if (line.size()==5)
                {
                    str.append(line.join(" "));
                    line.clear();
                }
            }
            if (!line.isEmpty())
            {
                str.append(line.join(" "));
            }
            gen.m_genContent = str.join("\n");
            input->Geometry = gen;
        }
        else if (node["Geometry"].IsPropertyList())
        {
            ExplicitGeometryNode geometrynode;
            if (node["Geometry"]["Periodic"].IsDefined())
            {
                QString value = QString::fromStdString(node["Geometry"]["Periodic"].as<std::string>());
                geometrynode.Periodic = (value.toLower() == "yes");
            }
            if (geometrynode.Periodic)
            {
                if (!node["Geometry"]["LatticeVectors"].IsDefined())
                {
                    throw MissingChildException("LatticeVectors");
                }
            }
            input->Geometry = geometrynode;
        }
    }




    return true;
}


void DFTBPlusInputReader::setFilename(const QString &fileName)
{
    setDevice(new QFile(fileName));
    deleteDevice = true;
}

void DFTBPlusInputReader::setDevice(QIODevice *device)
{
    if (m_device && deleteDevice)
        delete m_device;
    m_device = device;
    deleteDevice = false;
}

}
