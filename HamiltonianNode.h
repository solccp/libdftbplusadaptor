#ifndef HAMILTONIANNODE_H
#define HAMILTONIANNODE_H

#include "BasicNodes.h"
#include "MixerNode.h"
#include "FillingNode.h"
#include "DispersionNode.h"

namespace DFTBPlus
{

    class SelectedShells : public Method
    {
    public:
         QVariantList m_selectedShells;
    public:
         void writeOut(const QString &key, NodeSerializer *serializer)
         {
             serializer->writeList(key, "SelectedShells", m_selectedShells);
         }
    };

    typedef boost::variant< QString, SelectedShells> MaxAngularMomentumValue;

    class MaxAngularMomentumNode : public Method
    {
    public:
        QMap<QString, MaxAngularMomentumValue> m_lmax;
    public:
        void writeOut(const QString &key, NodeSerializer *serializer)
        {
            serializer->writeStartNode(key, "");
            QMapIterator<QString, MaxAngularMomentumValue> it(m_lmax);
            while(it.hasNext())
            {
                it.next();

                MaxAngularMomentumValue value = it.value();
                if (value.type() == typeid(QString))
                {
                    serializer->writeElement(it.key(), boost::get<QString>(value));
                }
                else
                {
                    boost::get<SelectedShells>(value).writeOut(it.key(), serializer);
                }
            }
            serializer->writeEndNode();
        }
    };


    class Type2FileNamesNode : public Method
    {
    public:
          Type2FileNamesNode() :Method("Type2FileNames"){}
          QString Prefix;
          QString Separator;
          QString Suffix;
          bool LowerCaseTypeName = false;

    public:
          void writeOut(const QString &key, NodeSerializer *serializer)
          {
              serializer->writeStartNode(key, m_methodName);
              serializer->writeElement("Prefix",Prefix);
              serializer->writeElement("Separator",Separator);
              serializer->writeElement("Suffix",Suffix);
              serializer->writeElement("LowerCaseTypeName",boolToYesNo(LowerCaseTypeName));
              serializer->writeEndNode();
          }
    };

    typedef boost::variant< QVariantMap, Type2FileNamesNode> SlaterKosterFilesNode;

    class SlaterKosterFilesNodeSerializer : public boost::static_visitor<>
    {
    public:
        SlaterKosterFilesNodeSerializer(NodeSerializer* serializer): m_serializer(serializer)
        {}

        void operator()(QVariantMap& node) const
        {
            m_serializer->writeMap("SlaterKosterFiles", "", node);
        }

        void operator()(Type2FileNamesNode& node) const
        {
            node.writeOut("SlaterKosterFiles", m_serializer);
        }
    private:
        NodeSerializer* m_serializer;
    };








    class DFTBHamiltonianNode : public Method
    {
    public:
        DFTBHamiltonianNode()
        {
            m_methodName = "DFTB";
        }
    public:
        bool SCC = false;
        double SCCTolerance = 1.0e-5;
        int MaxSCCIterations = 100;
        double EwaldParameter = 0.0;
        bool OrbitalResolvedSCC = false;
        //Mixer
        MixerNode Mixer = BroydenMixerNode();
        MaxAngularMomentumNode MaxAngularMomentum;
        double Charge = 0.0;
        //SpinPolarisation
        //SpinConstants
        //SpinOrbital
        //EigenSolver
        //Filling
        FillingNode Filling = FermiFillingNode();
        bool IndependentKFilling = false;
        //SlaterKosterFiles
        SlaterKosterFilesNode SlaterKosterFiles;
        bool OldSKInterpolation = false;
        //PolynomialRepulsive
        bool OldRepulsiveSum = false;
        //KPointsAndWeights
        //OrbitalPotential
        bool ReadInitialCharges = false;
        //InitialCharges
        //ElectricField
        //Dispersion
        DispersionNode Dispersion = NullNode();


        bool DampXH = false;
        double DampXHExponent = 4.0;
        bool ThirdOrder = false;
        bool ThirdOrderFull = false;
        QMap<QString, QVariant> HubbardDerivs;

        // DFTBPlusInputNode interface
    public:
        void writeOut(const QString& key, NodeSerializer *serializer)
        {
            serializer->writeStartNode(key, "DFTB");
            serializer->writeElement("SCC", boolToYesNo(SCC));
            if (SCC)
            {
                serializer->writeElement("SCCTolerance", SCCTolerance);
                serializer->writeElement("MaxSCCIterations", MaxSCCIterations);

                serializer->writeElement("OrbitalResolvedSCC", boolToYesNo(this->OrbitalResolvedSCC));

                MixerNodeSerializer mixer_ser(serializer);
                boost::apply_visitor(mixer_ser, Mixer);

                serializer->writeElement("EwaldParameter", EwaldParameter);
                serializer->writeElement("ReadInitialCharges", boolToYesNo(ReadInitialCharges));

                serializer->writeElement("DampXH", boolToYesNo(DampXH));
                if (DampXH)
                {
                    serializer->writeElement("DampXHExponent", DampXHExponent);
                }
                serializer->writeElement("ThirdOrder", boolToYesNo(ThirdOrder));
                serializer->writeElement("ThirdOrderFull", boolToYesNo(ThirdOrderFull));
                if (ThirdOrder || ThirdOrderFull)
                {
                    serializer->writeMap("HubbardDerivs", "", HubbardDerivs);
                }
            }

            serializer->writeElement("Charge", Charge);
            MaxAngularMomentum.writeOut("MaxAngularMomentum", serializer);
            SlaterKosterFilesNodeSerializer sk_ser(serializer);
            boost::apply_visitor(sk_ser, SlaterKosterFiles);
            FillingNodeSerializer filling_ser(serializer);
            boost::apply_visitor(filling_ser, Filling);

            DispersionNodeSerializer dispersion_ser(serializer);
            boost::apply_visitor(dispersion_ser, Dispersion);

            serializer->writeEndNode();
        }
    };
}


#endif // HAMILTONIANNODE_H
