#ifndef DRIVERNODE_H
#define DRIVERNODE_H

#include "BasicNodes.h"

namespace DFTBPlus
{
    class NullDriver : public DFTBPlusInputNode
    {
    public:
        void writeOut(const QString& key, NodeSerializer *serializer)
        {
            serializer->writeStartNode(key, "");
            serializer->writeEndNode();
        }
    };


    class DriverNodeBase : public Method
    {
    protected:
        DriverNodeBase(){}
        QVariantList MovedAtoms = {"1:-1"};
        double MaxForceComponent = 1.0e-4;
        int MaxSteps = 200;       
        QString OutputPrefix = "geo_end";
        bool AppendGeometries = false;
        QVariantList Constraints;
        bool LatticeOpt = false;
        bool FixAngles = false;
        QVariantList FixLengths = {false, false, false};
        bool Isotropic = false;
        double Pressure = 0.0;
        bool ConvergentForcesOnly = true;
    public:
        void writeOut(const QString& key, NodeSerializer *serializer) final
        {
            serializer->writeStartNode(key, m_methodName);
            serializer->writeList("MovedAtoms", "", MovedAtoms);
            serializer->writeElement("MaxForceComponent", MaxForceComponent);

            serializer->writeElement("OutputPrefix", OutputPrefix);
            serializer->writeElement("AppendGeometries", boolToYesNo(AppendGeometries));

            serializer->writeElement("LatticeOpt", boolToYesNo(LatticeOpt));
            if (!LatticeOpt)
            {
                serializer->writeList("Constraints", "", Constraints, 4);
            }
            else
            {
                serializer->writeElement("FixAngles", boolToYesNo(FixAngles));
                if (FixAngles)
                {
                    serializer->writeList("FixLengths", "", FixLengths);
                }
                serializer->writeElement("Pressure", Pressure);
                serializer->writeElement("Isotropic", boolToYesNo(Isotropic));
            }
            serializer->writeElement("ConvergentForcesOnly", boolToYesNo(ConvergentForcesOnly));
            childrenWriteOut(serializer);
            serializer->writeEndNode();
        }
    };

    class SteepestDescentDriver : public DriverNodeBase
    {
    public:
        SteepestDescentDriver()
        {
            m_methodName = "SteepestDescent";
        }
    protected:
        double StepSize = 100.0;

        // DFTBPlusInputNode interface
    public:
        void childrenWriteOut(NodeSerializer *serializer)
        {
            serializer->writeElement("StepSize", StepSize);
        }
    };

    class ConjugateGradientDriver : public DriverNodeBase
    {
    public:
        ConjugateGradientDriver()
        {
            m_methodName = "LBFGS";
        }
    protected:
    };

    class SecondDerivativesDriver : public Method
    {
    public:
        SecondDerivativesDriver()
        {
            m_methodName = "SecondDerivatives";
        }
    private:
        QVariantList Atoms = {"1:-1"};
        double Delta = 1.0e-4;

        // DFTBPlusInputNode interface
    public:
        void writeOut(const QString& key, NodeSerializer *serializer)
        {
            serializer->writeStartNode(key, m_methodName);
            serializer->writeList("Atoms", "", Atoms);
            serializer->writeElement("Delta", Delta);
            serializer->writeEndNode();
        }
    };

    typedef boost::variant< NullDriver, SteepestDescentDriver
            ,ConjugateGradientDriver, SecondDerivativesDriver> DriverNode;

    class DriverNodeSerializer : public boost::static_visitor<>
    {
    public:
        DriverNodeSerializer(NodeSerializer* serializer): m_serializer(serializer)
        {}

        void operator()(NullDriver& node) const
        {
            Q_UNUSED(node);
//            node.writeOut("Driver", m_serializer);
        }

        void operator()(SteepestDescentDriver& node) const
        {
            node.writeOut("Driver", m_serializer);
        }

        void operator()(ConjugateGradientDriver& node) const
        {
            node.writeOut("Driver", m_serializer);
        }

        void operator()(SecondDerivativesDriver& node) const
        {
            node.writeOut("Driver", m_serializer);
        }

    private:
        NodeSerializer* m_serializer;
    };

}

#endif // DRIVERNODE_H
