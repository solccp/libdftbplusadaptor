#include "HSDNodeSerializer.h"

#include <QStringList>

#include <QStringBuilder>

namespace DFTBPlus
{

inline QString boolToYesNo(bool value)
{
    if (value)
    {
        return "Yes";
    }
    else
    {
        return "No";
    }
}


inline QString toQuoteString(const QVariant& var)
{
    if ( var.type() == QMetaType::QString && var.toString() != "Yes" &&
         var.toString() != "No" )
    {
        return '"' + var.toString() + '"';
    }
    else
    {
        return var.toString();
    }
}


static const QString indentSpace = "    ";

HSDNodeSerializer::HSDNodeSerializer(QIODevice *device)
{
    m_stream.setDevice(device);
}

HSDNodeSerializer::HSDNodeSerializer(QTextStream &stream)
{
    //    m_stream = stream;
    m_stream.setDevice(stream.device());
}

void HSDNodeSerializer::writeStartDocument(){}

void HSDNodeSerializer::writeComment(const QString &comment)
{
    m_stream << "#" << comment << endl;
}

void HSDNodeSerializer::writeEndNode()
{
    --indentLevel;
    m_stream << indentSpace.repeated(indentLevel) << "}" << endl;
}

void HSDNodeSerializer::writeElement(const QString &name, const QVariant &value, bool addQuote)
{
    if (!name.isEmpty())
    {
        m_stream << indentSpace.repeated(indentLevel) << name << " = ";
    }
    if (addQuote)
        m_stream << toQuoteString(value) << endl;
    else
        m_stream << value.toString() << endl;
}

void HSDNodeSerializer::writeStartNode(const QString &key, const QString &name)
{
    m_stream << indentSpace.repeated(indentLevel) << key << " = " << name << "{" << endl;
    ++indentLevel;
}

void HSDNodeSerializer::writeMap(const QString &keyname, const QString &mapName, const QVariantMap &map)
{
    if (!keyname.isEmpty())
    {
        m_stream << indentSpace.repeated(indentLevel) << keyname << " = ";
    }
    m_stream << mapName;
    if (map.isEmpty())
    {
        m_stream << "{}" << endl ;
        return ;
    }
    m_stream << "{" << endl;
    QMapIterator<QString, QVariant> it(map);
    while(it.hasNext())
    {
        it.next();
        m_stream << indentSpace.repeated(indentLevel+1) << it.key()
                 << " = " << toQuoteString(it.value())
                 << endl;
    }
    m_stream << indentSpace.repeated(indentLevel) << "}" << endl;
}

void HSDNodeSerializer::writeList(const QString &keyname, const QString& listName, const QVariantList &value, int columns)
{
    QVariantList list = value;
    if (!keyname.isEmpty())
    {
        m_stream << indentSpace.repeated(indentLevel) << keyname << " = ";
    }

    m_stream << listName;

    QStringList final_lines;

    bool single_item = false;

    if (value.isEmpty())
    {
        m_stream << "{}" << endl ;
        return ;
    }
    else if (value.size() == 1)
    {
        single_item = true;
    }


    int l_col = columns;
    if (columns == 0)
        l_col = list.size();

    while(!list.isEmpty())
    {
        QStringList strlist;
        while(!list.isEmpty() && strlist.size() < l_col)
        {
            QVariant item = list.takeFirst();
            if (item.type() == QVariant::Bool)
            {
                strlist.append(boolToYesNo(item.toBool()));
            }
            else
            {
                strlist.append(toQuoteString(item));
            }
        }
        QString line = strlist.join(" ");
        final_lines.append(line);
    }

    if (final_lines.size() == 1)
    {
        if (single_item)
        {
            m_stream << final_lines.first() << endl;
        }
        else
        {
            m_stream << "{ " << final_lines.first() << " }" << endl ;
        }

    }
    else
    {
        m_stream << "{" << endl;
        for(int i=0; i<final_lines.size();++i)
        {
            m_stream << indentSpace.repeated(indentLevel+1) << final_lines.at(i) << endl;
        }
        m_stream << indentSpace.repeated(indentLevel) << "}" << endl;
    }


}
}





