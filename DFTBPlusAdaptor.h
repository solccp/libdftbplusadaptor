#ifndef DFTBPLUSADAPTOR_H
#define DFTBPLUSADAPTOR_H

#include <QObject>
#include <QException>
#include "Singleton.h"

static void initResources2()
{
    Q_INIT_RESOURCE(data2);
}

namespace DFTBPlus
{
    class DFTBPlusError
    {
    public:
        enum ErrorType{
            NoError,
            ProgramNotConfiguredError,
            InputFileInvalidError,
            OutputFileError,
            SCCNotConvergedError,
            GeometryNotConvergedError,
            SKParameterInvalidError,
            UnknownError
        };
        DFTBPlusError(const QString& text = QString(), ErrorType errorType = NoError);
        ErrorType errorType() const
        {
            return m_errorType;
        }
        QString text() const
        {
            return m_text;
        }


    protected:
        QString m_text;
        ErrorType m_errorType;
    };


    class ProgramNotConfiguredException : QException
    {
    public:
        void raise() const { throw *this; }
        ProgramNotConfiguredException *clone() const { return new ProgramNotConfiguredException(*this); }
    };

class DFTBPlusAdaptor
{
    friend class Singleton<DFTBPlusAdaptor>;
public:
    enum Feature
    {
        NoSpecialFeature = 0x0,
        VibrationFrequencyFeature = 0x1,
        RepulsivePairsInAutotestTagFeature = 0x2,
        DipoleMomentFeature= 0x4,
        FourthOrderSplineRepulsiveFeature = 0x8
    };
    Q_DECLARE_FLAGS(Features, Feature)



    bool setProgramPath(const QString& program_path);
    QString version() const;
    bool hasFeature(Feature feature);
    Features features() const;

    //! fixme
    //! ugly implementation
    DFTBPlusError getTotalEnergy(const QString& inputFilePath, double *energy);


private:
    DFTBPlusAdaptor();

    QString m_program_path;
    bool m_programIsReady = false;
    Features m_features;
    QString m_version;

private:
    void testFeatures();
    void testVersion();
    void testFrequency();
    void testRepPairs();
    void testFourthOrderRepulsive();
    DFTBPlusError checkError(const QString& runningPath);

    enum EnergyType
    {
        TotalEnergy = 0,
        TotalElectronicEnergy,
        RepulsiveEnergy,
        DispersionEnergy
    };

    QRegularExpression getParsingKeyword(DFTBPlusAdaptor::EnergyType type);
    bool parseEnergy(const QString& runningPath, double *energy, EnergyType type = TotalEnergy);
};

Q_DECLARE_OPERATORS_FOR_FLAGS(DFTBPlusAdaptor::Features)

}

#endif // DFTBPLUSPROGRAM_H
