#-------------------------------------------------
#
# Project created by QtCreator 2014-02-24T18:06:21
#
#-------------------------------------------------

QT       += core
QT       -= gui

TARGET = DFTBPlusAdaptor
TEMPLATE = lib
CONFIG += staticlib c++11

SOURCES += \
    DFTBPlusInput.cpp \
    nodeserializer.cpp \
    HSDNodeSerializer.cpp \
    DFTBPlusInputReader.cpp \
    DFTBPlusAdaptor.cpp
HEADERS += \
    DFTBPlusInput.h \
    HamiltonianNode.h \
    BasicNodes.h \
    GeometryNode.h \
    DriverNode.h \
    nodeserializer.h \
    HSDNodeSerializer.h \
    MixerNode.h \
    OptionsNode.h \
    DFTBPlusInputReader.h \
    FillingNode.h \
    DFTBPlusAdaptor.h \
    Singleton.h \
    DispersionNode.h

unix:!macx: LIBS += -L$$OUT_PWD/../libHSDParser/ -lHSDParser

INCLUDEPATH += $$PWD/../libHSDParser/include $$PWD/../libHSDParser/boost
DEPENDPATH += $$PWD/../libHSDParser

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../libHSDParser/libHSDParser.a

RESOURCES += \
    data2.qrc
