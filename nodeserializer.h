#ifndef NODESERIALIZER_H
#define NODESERIALIZER_H

#include <QString>
#include <QVariant>

namespace DFTBPlus
{

class NodeSerializer
{    
public:
    virtual void writeStartDocument() = 0;
    virtual void writeEndDocument() = 0;
    virtual void writeComment(const QString& comment) = 0;
    virtual void writeStartNode(const QString& key, const QString& name) = 0;
    virtual void writeEndNode() = 0;
    virtual void writeElement(const QString & name, const QVariant & value, bool addQuote = true) = 0;
    virtual void writeList(const QString & keyname, const QString& listName, const QVariantList & value, int columns = 0) = 0;
    virtual void writeMap(const QString& keyname, const QString& mapName, const QVariantMap &map) = 0;
    virtual ~NodeSerializer(){}
};
}

#endif // NODESERIALIZER_H
