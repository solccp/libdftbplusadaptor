#ifndef DISPERSIONNODE_H
#define DISPERSIONNODE_H

#include "BasicNodes.h"

namespace DFTBPlus
{

class UFFParametersNode : public Method
{
public:
    UFFParametersNode():Method("UFFParameters"){}
    void writeOut(const QString &key, NodeSerializer *serializer)
    {
        serializer->writeStartNode(key, m_methodName);
        serializer->writeEndNode();
    }
};


typedef boost::variant< UFFParametersNode > ParametersNode;

class ParametersNodeSerializer : public boost::static_visitor<>
{
public:
    ParametersNodeSerializer(NodeSerializer* serializer): m_serializer(serializer)
    {}

    void operator()(UFFParametersNode& node) const
    {
        node.writeOut("Parameters", m_serializer);
    }
private:
    NodeSerializer* m_serializer;
};


class LennardJonesNode : public Method
{
public:
    LennardJonesNode():Method("LennardJones"){}
    ParametersNode Parameter = UFFParametersNode();

    void writeOut(const QString &key, NodeSerializer *serializer)
    {
         serializer->writeStartNode(key, m_methodName);
         ParametersNodeSerializer parameters_ser(serializer);
         boost::apply_visitor(parameters_ser, Parameter);
         serializer->writeEndNode();
    }
};

typedef boost::variant< NullNode, LennardJonesNode > DispersionNode;

class DispersionNodeSerializer : public boost::static_visitor<>
{
public:
    DispersionNodeSerializer(NodeSerializer* serializer): m_serializer(serializer)
    {}

    void operator()(LennardJonesNode& node) const
    {
        node.writeOut("Dispersion", m_serializer);
    }
    void operator()(NullNode& node) const
    {
        Q_UNUSED(node);
    }
private:
    NodeSerializer* m_serializer;
};

}

#endif // DISPERSIONNODE_H
