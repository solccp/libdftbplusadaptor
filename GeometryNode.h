#ifndef GEOMETRYNODE_H
#define GEOMETRYNODE_H

#include "BasicNodes.h"
#include <QList>


namespace DFTBPlus {

    class GenFormatGeometryNode : public Method
    {
    public:
        GenFormatGeometryNode()
        {
            m_methodName = "GenFormat";
        }
        QString m_genContent;

        // DFTBPlusInputNode interface
    private:
        int indentLevel() const
        {
            return 1;
        }

        // DFTBPlusInputNode interface
    public:
        void writeOut(const QString& key, NodeSerializer *serializer)
        {
            serializer->writeStartNode(key, "GenFormat");
            serializer->writeElement("", m_genContent, false);
            serializer->writeEndNode();
        }
    };


    class ExplicitGeometryNode : public DFTBPlusInputNode
    {
    public:
        bool Periodic = false;
        QList<QVariant> LatticeVectors;
        QList<QVariant> TypeNames;
        QVariantList TypeAndCoordinates;

        // DFTBPlusInputNode interface
    public:
        int indentLevel() const
        {
            return 1;
        }
        void writeOut(const QString& key, NodeSerializer *serializer)
        {
            serializer->writeStartNode(key, "");
            serializer->writeElement("Periodic", boolToYesNo(Periodic));
            serializer->writeList("LatticeVectors", "", LatticeVectors, 3);
            serializer->writeList("TypeNames", "", TypeNames);
            serializer->writeList("TypeAndCoordinates", "", TypeAndCoordinates, 4);
            serializer->writeEndNode();
        }
    };

    typedef boost::variant< GenFormatGeometryNode, ExplicitGeometryNode, NullNode> GeometryNode;

    class GeometryNodeSerializer : public boost::static_visitor<>
    {
    public:
        GeometryNodeSerializer(NodeSerializer* serializer): m_serializer(serializer)
        {}

        void operator()(GenFormatGeometryNode& node) const
        {
            node.writeOut("Geometry", m_serializer);
        }

        void operator()(ExplicitGeometryNode& node) const
        {
            node.writeOut("Geometry", m_serializer);
        }
        void operator()(NullNode& node) const
        {
            Q_UNUSED(node);
        }

    private:
        NodeSerializer* m_serializer;
    };

}


#endif // GEOMETRYNODE_H
