#ifndef DFTBPLUSINPUTREADER_H
#define DFTBPLUSINPUTREADER_H

#include <QObject>

QT_BEGIN_NAMESPACE
class QIODevice;
QT_END_NAMESPACE

namespace DFTBPlus
{

class DFTBPlusInput;
class Exception
{
public:
    explicit Exception(const QString& what)
    {
        m_what = what;
    }
    QString what() const
    {
        return m_what;
    }
protected:
    QString m_what;
};
class MissingChildException : public Exception
{
public:
    MissingChildException(const QString& childname)
        : Exception(childname)
    {

    }

};

class DFTBPlusInputReader : public QObject
{
    Q_OBJECT
public:
    explicit DFTBPlusInputReader(QObject *parent = 0);

signals:

public slots:

private:
    QIODevice* m_device;
    bool deleteDevice = false;
public:
    QString fileName() const;
    QIODevice* device() const;
    bool read(DFTBPlusInput* input);
    void setFilename(const QString& fileName);
    void setDevice(QIODevice* device);


};

}

#endif // DFTBPLUSINPUTREADER_H
