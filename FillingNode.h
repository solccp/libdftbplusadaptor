#ifndef FILLINGNODE_H
#define FILLINGNODE_H

#include "BasicNodes.h"

namespace DFTBPlus
{

    class FermiFillingNode : public Method
    {
    public:
        FermiFillingNode():Method("Fermi"){}
        double Temperature = 0.0;

        void writeOut(const QString &key, NodeSerializer *serializer)
        {
             serializer->writeStartNode(key, m_methodName);
             serializer->writeElement("Temperature",Temperature);
             serializer->writeEndNode();
        }
    };

    typedef boost::variant< FermiFillingNode > FillingNode;

    class FillingNodeSerializer : public boost::static_visitor<>
    {
    public:
        FillingNodeSerializer(NodeSerializer* serializer): m_serializer(serializer)
        {}

        void operator()(FermiFillingNode& node) const
        {
            node.writeOut("Filling", m_serializer);
        }
    private:
        NodeSerializer* m_serializer;
    };

}

#endif // FILLINGNODE_H
