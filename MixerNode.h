#ifndef MIXERNODE_H
#define MIXERNODE_H


#include <BasicNodes.h>

namespace DFTBPlus
{

    class BroydenMixerNode : Method
    {
    public:
        BroydenMixerNode() : Method("Broyden"){}
        double MixingParameter = 0.2;
        int CachedIterations = -1;
        double InverseJacobiWeight = 0.01;
        double MinimalWeight = 1.0;
        double MaximalWeight = 1e5;
        double WeightFactor = 1e-2;

    public:
        void writeOut(const QString &key, NodeSerializer *serializer)
        {
            serializer->writeStartNode(key, m_methodName);
            serializer->writeElement("MixingParameter", MixingParameter);
            serializer->writeElement("CachedIterations", CachedIterations);
            serializer->writeElement("InverseJacobiWeight", InverseJacobiWeight);
            serializer->writeElement("MinimalWeight", MinimalWeight);
            serializer->writeElement("MaximalWeight", MaximalWeight);
            serializer->writeElement("WeightFactor", WeightFactor);
            serializer->writeEndNode();
        }
    };
    class SimpleMixerNode : Method
    {
    public:
        SimpleMixerNode() : Method("Simple"){}
        double MixingParameter = 0.05;

        // DFTBPlusInputNode interface
    public:
        void writeOut(const QString &key, NodeSerializer *serializer)
        {
            serializer->writeStartNode(key, m_methodName);
            serializer->writeElement("MixingParameter", MixingParameter);
            serializer->writeEndNode();
        }
    };

    typedef boost::variant< BroydenMixerNode, SimpleMixerNode > MixerNode;



    class MixerNodeSerializer : public boost::static_visitor<>
    {
    public:
        MixerNodeSerializer(NodeSerializer* serializer): m_serializer(serializer)
        {}

        void operator()(BroydenMixerNode& node) const
        {
            node.writeOut("Mixer", m_serializer);
        }
        void operator()(SimpleMixerNode& node) const
        {
            node.writeOut("Mixer", m_serializer);
        }
    private:
        NodeSerializer* m_serializer;
    };

}



#endif // MIXERNODE_H
