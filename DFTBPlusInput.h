#ifndef DFTBPLUSINPUT_H
#define DFTBPLUSINPUT_H
#include <QString>
#include <QTextStream>


#include "HamiltonianNode.h"
#include "GeometryNode.h"
#include "DriverNode.h"
#include "OptionsNode.h"

namespace DFTBPlus
{
    class DFTBPlusInput
    {

    public:
        GeometryNode Geometry = GeometryNode(NullNode());
        DFTBHamiltonianNode Hamiltonian;
        DriverNode Driver = DriverNode(NullDriver());
        OptionsNode Options;
        ParserOptionsNode ParserOptions;

    public:
        void writeOut(NodeSerializer *serializer)
        {
            serializer->writeStartDocument();
            GeometryNodeSerializer geom_ser(serializer);
            boost::apply_visitor(geom_ser, Geometry);
            DriverNodeSerializer driver_ser(serializer);
            boost::apply_visitor(driver_ser, Driver);
            Hamiltonian.writeOut("Hamiltonian", serializer);
            Options.writeOut("Options", serializer);
            ParserOptions.writeOut("ParserOptions", serializer) ;
            serializer->writeEndDocument();
        }
    };

}

#endif // DFTBPLUSINPUT_H




