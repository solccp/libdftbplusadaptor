#ifndef HSDNODESERIALIZER_H
#define HSDNODESERIALIZER_H

#include "nodeserializer.h"
#include <QTextStream>


namespace DFTBPlus
{

class HSDNodeSerializer : public NodeSerializer
{
public:
    HSDNodeSerializer(QIODevice* device);
    HSDNodeSerializer(QTextStream& stream);

private:
    QTextStream m_stream;
    int indentLevel = 0;
    // NodeSerializer interface
public:
    void writeStartDocument();
    void writeEndDocument(){}
    void writeComment(const QString& comment);
    void writeEndNode();
    void writeElement(const QString &name, const QVariant &value, bool addQuote = true);
    void writeStartNode(const QString &key, const QString &name);
    void writeList(const QString &name, const QString& listName, const QVariantList &value, int columns);
    void writeMap(const QString &keyname, const QString &mapName, const QVariantMap &map);
};

}

#endif // HSDNODESERIALIZER_H
