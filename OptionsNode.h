#ifndef OPTIONSNODE_H
#define OPTIONSNODE_H

#include "BasicNodes.h"

namespace DFTBPlus
{
    class OptionsNode : public Method
    {
    public:
        bool MullikenAnalysis = false;
        bool CalculateForces = false;
        bool WriteEigenvectors = false;
        bool WriteAutotestTag = false;
        bool WriteDetailedXML = false;
        bool WriteResultsTag = false;
        bool WriteDetailedOut = true;
        bool WriteBandOut = true;
        bool AtomResolvedEnergies = false;
        int RestartFrequency = 20;
        int RandomSeed  = 0;
        bool MinimiseMemoryUsage = false;
        bool ShowFoldedCoords = false;
        bool WriteHS = false;
        bool WriteRealHS = false;

    public:
        void writeOut(const QString &key, NodeSerializer *serializer)
        {
            serializer->writeStartNode(key, m_methodName);
            serializer->writeElement("MullikenAnalysis", boolToYesNo(MullikenAnalysis));
            serializer->writeElement("CalculateForces", boolToYesNo(CalculateForces));
            serializer->writeElement("WriteEigenvectors", boolToYesNo(WriteEigenvectors));
            serializer->writeElement("WriteAutotestTag", boolToYesNo(WriteAutotestTag));
            serializer->writeElement("WriteDetailedXML", boolToYesNo(WriteDetailedXML));
            serializer->writeElement("WriteResultsTag", boolToYesNo(WriteResultsTag));
            serializer->writeElement("WriteDetailedOut", boolToYesNo(WriteDetailedOut));
            serializer->writeElement("WriteBandOut", boolToYesNo(WriteBandOut));
            serializer->writeElement("AtomResolvedEnergies", boolToYesNo(AtomResolvedEnergies));
            serializer->writeElement("MinimiseMemoryUsage", boolToYesNo(MinimiseMemoryUsage));
            serializer->writeElement("ShowFoldedCoords", boolToYesNo(ShowFoldedCoords));
            serializer->writeElement("WriteHS", boolToYesNo(WriteHS));
            serializer->writeElement("WriteRealHS", boolToYesNo(WriteRealHS));
            serializer->writeElement("RestartFrequency", RestartFrequency);
            serializer->writeElement("RandomSeed", RandomSeed);
            serializer->writeEndNode();
        }
    };


    class ParserOptionsNode : public Method
    {
    protected:
        int ParserVersion = 4;
        bool WriteHSDInput = true;
    public:
        bool WriteXMLInput = false;
        bool IgnoreUnprocessedNodes = false;
        bool StopAfterParsing = false;

        void writeOut(const QString &key, NodeSerializer *serializer)
        {
            serializer->writeStartNode(key, m_methodName);
            serializer->writeElement("ParserVersion", ParserVersion);
            serializer->writeElement("WriteHSDInput", boolToYesNo(WriteHSDInput));
            serializer->writeElement("WriteXMLInput", boolToYesNo(WriteXMLInput));
            serializer->writeElement("IgnoreUnprocessedNodes", boolToYesNo(IgnoreUnprocessedNodes));
            serializer->writeElement("StopAfterParsing", boolToYesNo(StopAfterParsing));
            serializer->writeEndNode();
        }
    };
}

#endif // OPTIONSNODE_H
