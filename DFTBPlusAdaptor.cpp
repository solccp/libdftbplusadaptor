#include "DFTBPlusAdaptor.h"

#include <QTemporaryDir>
#include <QProcess>

#include <QDebug>
#include <QRegularExpression>
#include <QTextStream>
#include <QStringBuilder>



namespace DFTBPlus
{



    DFTBPlusError::DFTBPlusError(const QString &text, DFTBPlusError::ErrorType errorType) : m_text(text), m_errorType(errorType)
    {

    }

    DFTBPlusAdaptor::DFTBPlusAdaptor()
    {
        initResources2();
    }

    void DFTBPlusAdaptor::testFeatures()
    {
        testFrequency();
//        testRepPairs();
        testFourthOrderRepulsive();
    }

    bool DFTBPlusAdaptor::setProgramPath(const QString &program_path)
    {
        m_program_path = program_path;
        testVersion();
        if (!m_programIsReady)
        {
            return false;
        }
        testFeatures();
        return true;
    }

    QString DFTBPlusAdaptor::version() const
    {
        if (!m_programIsReady)
        {
            throw ProgramNotConfiguredException();
        }
        return m_version;
    }

    bool DFTBPlusAdaptor::hasFeature(DFTBPlusAdaptor::Feature feature)
    {
        return m_features.testFlag(feature);
    }

    DFTBPlusAdaptor::Features DFTBPlusAdaptor::features() const
    {
        return m_features;
    }

    DFTBPlusError DFTBPlusAdaptor::getTotalEnergy(const QString &inputFilePath, double *energy)
    {
        QTemporaryDir tempDir_;
        QDir tempDir(tempDir_.path());

        bool succ = QFile::copy(inputFilePath, tempDir.absoluteFilePath("dftb_in.hsd"));

        if (!succ)
        {
            return DFTBPlusError("Copying input file error", DFTBPlusError::InputFileInvalidError);
        }

        QProcess m_process;
        m_process.setWorkingDirectory(tempDir.absolutePath());
        m_process.setStandardOutputFile(tempDir.absoluteFilePath("dftb_out.txt"));
        m_process.start(m_program_path);
        m_process.waitForFinished(-1);

        DFTBPlusError error = checkError(tempDir.absolutePath());
        if (error.errorType() == DFTBPlusError::NoError)
        {
            double l_energy;
            succ = parseEnergy(tempDir.absolutePath(), &l_energy);
            if (!succ)
            {
                return DFTBPlusError("Parsing detailed.out error", DFTBPlusError::OutputFileError);
            }
            *energy = l_energy;
        }
        return error;


    }

    void DFTBPlusAdaptor::testVersion()
    {
        QTemporaryDir tempDir_;
        if (!tempDir_.isValid())
        {
            this->m_programIsReady = false;
            return;
        }
        QDir tempDir(tempDir_.path());

        QProcess m_process;
        m_process.setWorkingDirectory(tempDir.absolutePath());

        m_process.start(m_program_path);
        m_process.waitForFinished(-1);

        QByteArray output = m_process.readAllStandardOutput();

        {
            QRegularExpression regex(".*DFTB\\+ paramerization version by Chien-Pin Chou:\\s*(?<version>.*)");
            QRegularExpressionMatch match = regex.match(QString(output));
            if (match.hasMatch())
            {
                m_version = match.captured("version");
                m_programIsReady = true;
                return;
            }
        }
        m_programIsReady = false;
    }

    void DFTBPlusAdaptor::testFrequency()
    {
        QTemporaryDir tempDir_;
        QDir tempDir(tempDir_.path());
//        tempDir_.setAutoRemove(false);

        QProcess m_process;
        m_process.setWorkingDirectory(tempDir.absolutePath());

        QFile::copy(":/data/hh.spl", tempDir.absoluteFilePath("hh.spl"));
        QFile::copy(":/data/testFreq.hsd",tempDir.absoluteFilePath("dftb_in.hsd"));

        m_process.start(m_program_path);
        m_process.waitForFinished(-1);


        QByteArray output = m_process.readAllStandardOutput();

        QRegularExpression regex("Vibrational modes \\(cm-1\\):(?<freq>.*)^\\s*$",
                                 QRegularExpression::DotMatchesEverythingOption |
                                 QRegularExpression::MultilineOption);

        QRegularExpressionMatch match = regex.match(QString(output));
        if (match.hasMatch())
        {
            bool ok;
            double freq = match.captured("freq").trimmed().toDouble(&ok);

            if (ok && freq >4420 && freq < 4425)
            {
                this->m_features |= Feature::VibrationFrequencyFeature;
            }
        }
    }

    void DFTBPlusAdaptor::testRepPairs()
    {
        QTemporaryDir tempDir_;
        QDir tempDir(tempDir_.path());
        tempDir_.setAutoRemove(false);

        QProcess *m_process = new QProcess();
        m_process->setWorkingDirectory(tempDir.absolutePath());

        QFile::copy(":/data/hh.spl", tempDir.absoluteFilePath("hh.spl"));
        QFile::copy(":/data/testRepPairs.hsd",tempDir.absoluteFilePath("dftb_in.hsd"));

        m_process->start(m_program_path);
        m_process->waitForFinished(-1);

        delete m_process;
        QFile file(tempDir.absoluteFilePath("autotest.tag"));
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {

            return;
        }

        QTextStream stream(&file);

        bool foundRepPairs = false;
        bool foundRepDistance = false;
        bool foundRepType = false;
        bool foundRepNeighbor = false;

        while(!stream.atEnd())
        {
            QString str = stream.readLine();
            if (str.startsWith("rep_pairs"))
            {
                foundRepPairs = true;
                break;
            }
            else if (str.startsWith("rep_type"))
            {
                foundRepType = true;
                break;
            }
            else if (str.startsWith("rep_neighbor"))
            {
                foundRepNeighbor = true;
                break;
            }
            else if (str.startsWith("rep_distance"))
            {
                foundRepDistance = true;
                break;
            }
        }

        if (foundRepPairs && foundRepType && foundRepNeighbor && foundRepDistance)
        {
            this->m_features |= Feature::RepulsivePairsInAutotestTagFeature;
        }
    }

    void DFTBPlusAdaptor::testFourthOrderRepulsive()
    {
        QTemporaryDir tempDir_;
        QDir tempDir(tempDir_.path());
        tempDir_.setAutoRemove(false);

        QProcess m_process;
        m_process.setWorkingDirectory(tempDir.absolutePath());

        QFile::copy(":/data/hh_4orderspline.spl", tempDir.absoluteFilePath("hh.spl"));
        QFile::copy(":/data/test4orderspline.hsd",tempDir.absoluteFilePath("dftb_in.hsd"));

        m_process.start(m_program_path);
        m_process.waitForFinished(-1);


        QByteArray output = m_process.readAllStandardOutput();

        //check error
        QTextStream stream(&output);
        bool foundError = false;
        while(!stream.atEnd())
        {
            if (stream.readLine().contains("ERROR!"))
            {
                foundError = true;
                break;
            }
        }

        if (!foundError)
        {
            this->m_features |= Feature::FourthOrderSplineRepulsiveFeature;
        }
    }

    DFTBPlusError DFTBPlusAdaptor::checkError(const QString &runningPath)
    {
        QDir dir(runningPath);
        QFile file(dir.absoluteFilePath("dftb_out.txt"));
        if (!file.open(QIODevice::Text | QIODevice::ReadOnly))
        {
            return DFTBPlusError("Cannot open output file: " + dir.absoluteFilePath("dftb_out.txt"), DFTBPlusError::OutputFileError);
        }

        QTextStream stream(&file);
        while(!stream.atEnd())
        {
            QString line = stream.readLine();
            if (line.contains("ERROR"))
            {
                while(!stream.atEnd())
                {
                    line += "\n" + stream.readLine() ;
                }
                //fixme, specify the error type not implemented
                return DFTBPlusError(line, DFTBPlusError::UnknownError);
            }
        }
        return DFTBPlusError("Success", DFTBPlusError::NoError);
    }

    QRegularExpression DFTBPlusAdaptor::getParsingKeyword(DFTBPlusAdaptor::EnergyType type)
    {
        switch(type)
        {
        case DFTBPlusAdaptor::TotalEnergy:
            return QRegularExpression("Total energy:\\s*(?<energy>.*?) H");
        case DFTBPlusAdaptor::DispersionEnergy:
            return QRegularExpression("Dispersion energy:\\s*(?<energy>.*?) H");
        case DFTBPlusAdaptor::TotalElectronicEnergy:
            return QRegularExpression("Total Electronic energy:\\s*(?<energy>.*?) H");
        case DFTBPlusAdaptor::RepulsiveEnergy:
            return QRegularExpression("Repulsive energy:\\s*(?<energy>.*?) H");
        default:
            Q_ASSERT(0);
        }
    }

    bool DFTBPlusAdaptor::parseEnergy(const QString &runningPath, double* energy, EnergyType type)
    {
        QDir dir(runningPath);
        QFile file(dir.absoluteFilePath("detailed.out"));
        if (!file.open(QIODevice::Text | QIODevice::ReadOnly))
        {
            return false;
        }

        QTextStream stream(&file);
        QRegularExpression regex = getParsingKeyword(type);


        while(!stream.atEnd())
        {
            QString line = stream.readLine();
            QRegularExpressionMatch match = regex.match(line);
            if (match.hasMatch())
            {
                bool ok;
                QString energy_str = match.captured("energy");
                double l_energy = energy_str.toDouble(&ok);
                if (ok)
                {
                    *energy = l_energy;
                    return true;
                }
                return false;
            }
        }
        return false;
    }
}

