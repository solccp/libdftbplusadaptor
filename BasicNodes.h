#ifndef BASICNODES_H
#define BASICNODES_H

#include <QString>
#include <QTextStream>
#include <QVariant>
#include <QList>

#include <boost/variant.hpp>

#include "nodeserializer.h"

namespace DFTBPlus
{


    inline QString boolToYesNo(bool value)
    {
        if (value)
        {
            return "Yes";
        }
        else
        {
            return "No";
        }
    }

    class DFTBPlusInputNode
    {
    public:
        virtual ~DFTBPlusInputNode(){}
        virtual void writeOut(const QString& key, NodeSerializer* serializer) = 0;
        virtual void childrenWriteOut(NodeSerializer*){}
    };

    class Method : public DFTBPlusInputNode
    {

    protected:
        QString m_methodName;
        Method(const QString& methodName = "") : m_methodName(methodName){}
    public:
        virtual ~Method(){}
    };

    class NullNode : public DFTBPlusInputNode
    {
        // DFTBPlusInputNode interface
    public:
        void writeOut(const QString &key, NodeSerializer *serializer)
        {
            Q_UNUSED(key);
            Q_UNUSED(serializer);
        }

    };
}

#endif // BASICNODES_H
